" Switch syntax highlighting on when the terminal has colors
" Also switch on highlighting  the last used search pattern
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
  set guifont=Luxi\ Mono\ 12
endif

if has("cursorline")
  set cursorline
endif

colo elflord

set incsearch " incremental search when typing
set ignorecase " ignore case when searching

set nocompatible " no compatible with previous versions
set number " show line number
set background=dark " background color
"syntax on " syntax enabled
filetype on " filetype checking enabled

set ai " autoindent
" set si " smartindent
set ci " cindent: indent for C

set cino+=:2
set cino+==0
"set cino+=:4
"set cino+==0

set tabstop=4 " 2 spaces for tab
set shiftwidth=4 "
" set expandtab " use space to replace tab

set ruler " show position of cursor all the time

set matchpairs=(:),{:},[:],<:>
set showmatch " highlight bracket matching
"set history=1000 "

set backspace=indent,eol,start

set mouse=a " enable mouse all the time

" Basically you press * or # to search for the current selection !! Really useful
vnoremap <silent> * :call VisualSearch('f')<CR>
vnoremap <silent> # :call VisualSearch('b')<CR>

" enable mouse, me=mouse enabled
map me :set mouse=a<CR><Esc>
" disable mouse, md=mouse disabled
map md :set mouse=<CR><Esc>

" toggle expandtab switch
" te=tab enabled
" td=tab disabled
map te :set noexpandtab<CR><Esc>
map td :set expandtab<CR><Esc>
