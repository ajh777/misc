# Set in ~/.bashrc

GIT_PROMPT=~/.git-prompt.sh
if [ "$color_prompt" = yes ]; then
    if [ ! -f ${GIT_PROMPT} ]; then
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    else
        . ${GIT_PROMPT}
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;31m\] $(__git_ps1 "(%s)")\[\033[00m\]\n\$ '
    fi
else
    if ! test -f ${GIT_PROMPT}; then
        PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
    else
        # git-prompt
        . ${GIT_PROMPT}
        PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w $(__git_ps1 "(%s)")\n\$ '
    fi
fi
